extern crate fyyd_types;
extern crate reqwest;

pub mod v2;

mod lib {
    pub use super::v2;
}
